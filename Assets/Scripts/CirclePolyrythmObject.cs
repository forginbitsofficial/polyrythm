﻿using UnityEngine;

public class CirclePolyrythmObject : PolyrythmObjectBase
{
    [SerializeField] public float radius;

    [SerializeField] private LineRenderer lineRenderer;

    protected override void StartActions()
    {
        DrawTrajectory();
    }

    private void DrawTrajectory()
    {
        var trajectoryPointCount = 360;
        
        var positions = new Vector3[trajectoryPointCount];

        for (var i = 0; i < trajectoryPointCount - 1; i++)
        {
            var angle = 2 * i * Mathf.PI / 360f;
            positions[i] = new Vector3(radius * Mathf.Cos(angle), 0f, radius * Mathf.Sin(angle));
        }
        
        // CLOSE TRAJECTORY
        positions[trajectoryPointCount - 1] = new Vector3(radius * Mathf.Cos(0), 0f, radius * Mathf.Sin(0));

        lineRenderer.positionCount = trajectoryPointCount;
        lineRenderer.SetPositions(positions);
    }

    protected override Vector3 ComputePosition(float elapsedTime)
    {
        var alpha = 2 * Mathf.PI * elapsedTime / frequency;
        return radius * new Vector3(Mathf.Cos(alpha), 0f, Mathf.Sin(alpha));
    }
}