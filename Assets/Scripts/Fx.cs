﻿using System;
using UnityEngine;

[Serializable]
internal record Fx
{
    public GameObject fxPrefab;
    public bool worldPositionStays;
    public float scale;
}