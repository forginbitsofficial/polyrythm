﻿using System.Collections.Generic;
using UnityEngine;

public abstract class PolyrythmObjectBase : MonoBehaviour
{
    [Range(0.01f, 100f)]
    [SerializeField] public float frequency;

    [SerializeField] public float offset;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private List<Fx> fxs;

    [SerializeField] public AudioClip audioClip;

    private float _currentTime;
    private float _previousTime;

    protected abstract Vector3 ComputePosition(float elapsedTime);

    protected abstract void StartActions();

    protected void Start()
    {
        StartActions();
        _currentTime = offset * frequency;
    }

    public void UpdateObject(float deltaTime)
    {
        _currentTime += deltaTime;

        if (_previousTime < frequency && _currentTime > frequency)
        {
            PlaySound();
            PlayFX();
            _currentTime -= frequency;
        }

        transform.position = ComputePosition(_currentTime);
    }

    private void PlayFX()
    {
        foreach (var fx in fxs)
        {
            if (fx.fxPrefab != null)
            {
                var instanceFx = Instantiate(fx.fxPrefab, transform.position, Quaternion.identity);
                instanceFx.transform.localScale *= fx.scale;

                if (!fx.worldPositionStays)
                {
                    instanceFx.transform.SetParent(transform);
                }
            }
        }
    }

    private void PlaySound()
    {
        if (audioClip != null)
        {
            audioSource.PlayOneShot(audioClip);
        }
    }
}