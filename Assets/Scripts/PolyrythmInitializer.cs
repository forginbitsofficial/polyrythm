using System.Collections.Generic;
using UnityEngine;

public class PolyrythmInitializer : MonoBehaviour
{
    [SerializeField] private List<AudioClip> audioClips;

    [SerializeField] private float startRadius;
    [SerializeField] private float endRadius;

    [SerializeField] private float completeCycleTime;
    [SerializeField] private float startDivider;
    [SerializeField] private bool invertFrequency;

    [SerializeField] private PolyrythmManager polyrythmManager;

    [ContextMenu("Initialize all parameters")]
    private void InitializeParameters()
    {
        InitializeRadius();
        InitializeAudioclips();
        InitializeFrequency();
    }

    [ContextMenu("Initialize audio clips")]
    private void InitializeAudioclips()
    {
        var polyrythmObjects = polyrythmManager.GetPolyrythmObjects();

        for (var i = 0; i < polyrythmObjects.Count; i++)
        {
            var polyrythmObject = polyrythmObjects[i];

            if (audioClips.Count > i)
            {
                polyrythmObject.audioClip = audioClips[i];
            }
        }
    }

    [ContextMenu("Initialize radius")]
    private void InitializeRadius()
    {
        var polyrythmObjects = polyrythmManager.GetPolyrythmObjects();

        for (var i = 0; i < polyrythmObjects.Count; i++)
        {
            var polyrythmObject = polyrythmObjects[i];

            if (polyrythmObjects.Count > 1)
            {
                polyrythmObject.radius = startRadius + i * (endRadius - startRadius) / (polyrythmObjects.Count - 1);
            }
        }
    }

    [ContextMenu("Initialize frequency")]
    private void InitializeFrequency()
    {
        var polyrythmObjects = polyrythmManager.GetPolyrythmObjects();

        for (var i = 0; i < polyrythmObjects.Count; i++)
        {
            var polyrythmObject = invertFrequency ? polyrythmObjects[polyrythmObjects.Count - i - 1] : polyrythmObjects[i];

            if (polyrythmObjects.Count > 1)
            {
                polyrythmObject.frequency = completeCycleTime /  (startDivider + i);
            }
        }
    }
}