using System.Collections.Generic;
using UnityEngine;

public class PolyrythmManager : MonoBehaviour
{
    [SerializeField] private List<CirclePolyrythmObject> polyrythmObjects;

    public List<CirclePolyrythmObject> GetPolyrythmObjects()
    {
        return polyrythmObjects;
    }
    
    // Update is called once per frame
    private void Update()
    {
        UpdateObjects(Time.deltaTime);
    }

    private void UpdateObjects(float deltaTime)
    {
        foreach (var polyrythmObject in polyrythmObjects)
        {
            polyrythmObject.UpdateObject(deltaTime);
        }
    }

    [ContextMenu("Add 60 seconds")]
    private void Add60Seconds()
    {
        UpdateObjects(60);
    }
    
    [ContextMenu("Add 300 seconds")]
    private void Add300Seconds()
    {
        UpdateObjects(300);
    }
}